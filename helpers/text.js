export default {
    title_landing: 'Welcome to Uno.',
    title_register: 'Create Account?',
    route_register: 'Register',
    route_login: 'Login',
    title_select_app: 'Select your favorite App',
    title_create_app: 'Create new App',

    text1_policy: 'By tapping Continue I agree to Airbnb',
    text2_policy: 'Terms of Service',
    text3_policy: 'Payments Terms of Service',
    text4_policy: 'Privacy Policy',
    text5_policy: 'and',
    text6_policy: 'Nondiscrimination Policy',

    title_Terms_policy: 'Terms of Service',
    title_Payments_policy: 'Payments Terms of Service',
    title_Privacy_policy: 'Privacy Policy',
    title_Nondiscrimination_policy: 'Nondiscrimination Policy',
    

  };
  