import React from 'react'
import {
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    ScrollView,
    Button
} from 'react-native'

//import Icon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons'
import { Avatar } from 'react-native-paper'
import { useNavigation } from '@react-navigation/native';

import colors from '../styles/colors'
import NavBarButton from '../components/buttons/NavBarButton'
import RoundedButton from '../components/buttons/RoundedButton'
import styles from '../styles/Landing'
//import texts from './helpers/text'
import { LinearGradient } from 'expo-linear-gradient';

//LOGIN
function GoToButton (props) {
    const navigation = useNavigation();
  
    const { screenName, content  } = props
  
    return (
    <TouchableOpacity 
      style={styles.moreOptionsButton}
      onPress={() => navigation.navigate(screenName)}>
      <Text  
       style={styles.moreOptionsButtonText}> 
       {content}
      </Text>  
    </TouchableOpacity>
    );
  }



 function Landing({ navigation }) {
    /*static navigationOptions = ({ navigation }) => ({
        headerRight: <NavBarButton handleButtonPress={() => navigation.navigate('Login')} location="right" color={colors.white} text="Log In" />,
        headerStyle: transparentHeaderStyle,
        headerTransparent: true,
        headerTintColor: colors.white,
      });*/

    const Logo = require('../img/medusa.jpg');
    const title = 'Welcome to Uno.'
    const content='Create Account?'
    const screenName = 'Register'
    const title_select_app = 'Select your favorite App'
    const title_create_app = 'Create new App'

    const text1_policy = 'By tapping Continue I agree to Airbnb'
    const text2_policy = 'Terms of Service'
    const text3_policy = 'Payments Terms of Service'
    const text4_policy = 'Privacy Policy'
    const text5_policy = 'and'
    const text6_policy = 'Nondiscrimination Policy'

    return (
        <ScrollView style={styles.wrapper}>
            <LinearGradient
             //colors={['#4c669f', '#3b5998', '#192f6a']}       
              colors={[ colors.gradientLight, colors.gradientMedium, colors.gradientDark]}
            >
                <View style={styles.welcomeWrapper}>
                  {/*<Image 
                        source={require('../img/Logo.png')}
                        style={styles.logo}
                    /> */} 
                    <Avatar.Image
                         size={69}
                         source={Logo}
                         style={styles.logo}
                    />
                     <Text style={styles.welcomeText}>
                           {title}
                    </Text> 
                     <RoundedButton 
                        text={title_select_app}
                        textColor={colors.green01}
                        background={colors.white}
                        icon={<Icon 
                                name="apps" 
                                size={20}
                                style={styles.AppButtonIcon}
                            />}
                        handleOnPress={() => navigation.navigate("Select_App")}
                    /> 
                    <RoundedButton 
                        text={title_create_app}
                        textColor={colors.white}
                        handleOnPress={() => navigation.navigate("Created_App")}
                    /> 
        
                    <GoToButton screenName = {screenName}  content= {content}/>

                    <View 
                        style={styles.termsAndConditions}
                    >
                        {/*<Text style={styles.termsText}>By tapping Continue, Create Account ot More </Text>
                        <Text style={styles.termsText}>options, </Text>*/}
                        <Text style={styles.termsText}>{text1_policy}</Text>
                        <TouchableHighlight style={styles.linkButton} onPress = {() => navigation.navigate("TermsofServices")}>
                            <Text style={styles.termsText}>{text2_policy}</Text>
                        </TouchableHighlight>
                        <Text style={styles.termsText}>, </Text>
                        <TouchableHighlight style={styles.linkButton} onPress = {() => navigation.navigate("PaymentsTofS")}>
                            <Text style={styles.termsText}>{text3_policy}</Text>
                        </TouchableHighlight>
                        <Text style={styles.termsText}>, </Text>
                        <TouchableHighlight style={styles.linkButton} onPress = {() => navigation.navigate("PrivacyPolicy")}>
                            <Text style={styles.termsText}>{text4_policy}</Text>
                        </TouchableHighlight>
                        <Text style={styles.termsText}>, {text5_policy}</Text>
                        <TouchableHighlight style={styles.linkButton} onPress = {() => navigation.navigate("NoPolicy")}>
                            <Text style={styles.termsText}>{text6_policy}</Text>
                        </TouchableHighlight>
                        <Text style={styles.termsText}>.</Text>
                    </View> 
                </View>
            </LinearGradient>
        </ScrollView>
    )
}




export default Landing