
import React, { useState } from 'react'
import { PropTypes } from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import { LinearGradient } from 'expo-linear-gradient';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    KeyboardAvoidingView
} from 'react-native'


import { transparentHeaderStyle } from '../../styles/navigation'
import colors from '../../styles/colors'
import InputField from '../../components/form/InputField'
import NextArrowButton from '../../components/buttons/NextArrowButton'
import Notification from '../../components/Notification'
import Loader from '../../components/Loader'
import NavBarButton from '../../components/buttons/NavBarButton'
import styles from '../../styles/Login'

function Login() {

    const [formValid , setFormValid ] = useState(true)
    const [validEmail , setValidEmail ] = useState(false)
    const [emailAddress , setEmailAddress ] = useState('')
    const [password , setPassword ] = useState('')
    const [validPassword , setValidPassword ] = useState(false);
    const [loadingVisible , setLoadingVisible ] = useState(false)
 
    const showNotification = !formValid
    const background = formValid ? colors.green01: colors.darkOrange
    const notificationMarginTop = showNotification ? 10 : 0

    const handleEmailChange = () => {
     
        console.log("Oka")
    }

    const handlePasswordChange = () => {
     
        console.log("Oka")
    }

    const handleNextButton = () => {
     
        console.log("Oka")
    }

    const handleCloseNotification = () => {
     
        console.log("Oka")
    }

    const  toggleNextButtonState = () => {
     
        console.log("Oka")
    }

    return (
    
       <KeyboardAvoidingView
        style={[{ backgroundColor: background }, styles.wrapper]}
        behavior="padding"
       >
 
        <View style={styles.scrollViewWrapper}>
      
          <ScrollView style={styles.scrollView}>
            <Text style={styles.loginHeader}>
Log In
            </Text>
             <InputField
              labelText="EMAIL ADDRESS"
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="email"
              customStyle={{ marginBottom: 30 }}
              onChangeText={handleEmailChange}
              showCheckmark={validEmail}
              autoFocus
             /> 
            <InputField
              labelText="PASSWORD"
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="password"
              customStyle={{ marginBottom: 30 }}
              onChangeText={handlePasswordChange}
              showCheckmark={validPassword}
           />
           
          </ScrollView>
          
          <NextArrowButton
            handleNextButton={handleNextButton}
            disabled={toggleNextButtonState}
          />
       
        </View>
       {/* <Loader
          modalVisible={loadingVisible}
          animationType="fade"
       />*/}
        <View style={[styles.notificationWrapper, { marginTop: notificationMarginTop }]}>
          <Notification
            showNotification={showNotification}
            handleCloseNotification={handleCloseNotification}
            type="Error"
            firstLine="Those credentials don't look right."
            secondLine="Please try again."
          />
         </View>
     
      </KeyboardAvoidingView>
      
    )
}


Login.propTypes = {
      //logIn: PropTypes.func.isRequired,
      navigation: PropTypes.shape({
      navigate: PropTypes.func,
      goBack: PropTypes.func,
    }).isRequired,
  }


export default Login