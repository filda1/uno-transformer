import React, { useState } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    View,
    Text,
    KeyboardAvoidingView,
    ScrollView,
  } from 'react-native';


import transparentHeaderStyle from '../../styles/navigation';
import colors from '../../styles/colors'
import InputField from '../../components/form/InputField';
import Notification from '../../components/Notification';
import NextArrowButton from '../../components/buttons/NextArrowButton';
import NavBarButton from '../../components/buttons/NavBarButton';
import Loader from '../../components/Loader';
import styles from '../../styles/ForgotPassword';

function ForgotPassword() {

   const handleEmailChange = (email) => {
   }

   const goToNextStep = () => {
   }

   const handleCloseNotification = () => {
   }

    const [formValid , setFormValid ] = useState(true)
    const [validEmail , setValidEmail ] = useState(false)
    const [emailAddress , setEmailAddress ] = useState('')
    const [loadingVisible , setLoadingVisible ] = useState(false)

    const background = formValid ? colors.green01 : colors.darkOrange;
    const showNotification = !formValid;

    return (
        <KeyboardAvoidingView
        style={[{ backgroundColor: background }, styles.wrapper]}
        behavior="padding"
      >
        <View style={styles.scrollViewWrapper}>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.forgotPasswordHeading}>
Forgot your password?
            </Text>
            <Text style={styles.forgotPasswordSubheading}>
Enter your email to find your account
            </Text>
            <InputField
              customStyle={{ marginBottom: 30 }}
              textColor={colors.white}
              labelText="EMAIL ADDRESS"
              labelTextSize={14}
              labelColor={colors.white}
              borderBottomColor={colors.white}
              inputType="email"
              onChangeText={handleEmailChange}
              showCheckmark={validEmail}
            />
          </ScrollView>
          <NextArrowButton
            handleNextButton={goToNextStep}
            disabled={!validEmail}
          />
        </View>
        {/*<Loader
          modalVisible={loadingVisible}
          animationType="fade"
        />*/}
        <View style={styles.notificationWrapper}>
          {/*<Notification
            showNotification={showNotification}
            handleCloseNotification={handleCloseNotification}
            type="Error"
            firstLine="No account exists for the requested"
            secondLine="email address."
          />*/}
        </View>
      </KeyboardAvoidingView>
    )
}



export default ForgotPassword