import { StatusBar } from 'expo-status-bar';
import React, {useEffect} from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
import { useNavigation,  NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AsyncStorage from '@react-native-community/async-storage';
import keys from './constants/Keys';
import Parse from "parse/react-native.js";

import NavBarButton from './components/buttons/NavBarButton'
import transparentHeaderStyle from './styles/navigation';
import colors from './styles/colors'
import texts from './helpers/text'
import LandingScreen from './screens/Landing'
import LoginScreen from './screens/auth/Login'
import RegisterScreen from './screens/auth/Register'
import ForgotPasswordScreen from './screens/auth/ForgotPassword'
import Select_AppScreen from './screens/Select_App'
import Created_AppScreen from './screens/Created_App'
import NoPolicyScreen from './screens/policy/NoPolicy'
import PaymentsTofSScreen from './screens/policy/PaymentsTofS'
import PrivacyPolicyScreen from './screens/policy/PrivacyPolicy'
import TermsofServicesScreen from './screens/policy/TermsofServices'


Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(keys.applicationId, keys.javascriptKey);
Parse.serverURL = keys.serverURL;

const Stack = createStackNavigator()


function App() {

  const Terms_policy = 'Terms of Service'
  const Payments_policy = 'Payments Terms of Service'
  const Privacy_policy = 'Privacy Policy'
  const Nondiscrimination_policy = 'Nondiscrimination Policy'

  useEffect(() => {
   /* const createInstallation = async () => {
      const Installation = Parse.Object.extend(Parse.Installation);
      const installation = new Installation();
  
      installation.set("deviceType", Platform.OS);
  
      await installation.save();
    }

   createInstallation();*/

   /* async function createUser(){
      const user = new Parse.User()
      user.set("username","test4")
      user.set("password","123456")
      user.set("email", "test4@gmail.com")
      user.set("telephone", 123456)

      
      try {
        await user.signUp()
        alert("User created")
      } catch (error) {
        alert(`ERROR: ${error.message}`)
        
      }
    }
   
    createUser()*/

  }, []);

  return (
    <NavigationContainer >
      <Stack.Navigator initialRouteName="Landing">
        <Stack.Screen 
          name="Landing" 
          component={LandingScreen}
          options={{ 
             headerTransparent: true,
             headerTitle: "",
             headerTintColor: colors.white,
             headerStyle: transparentHeaderStyle,
             headerRight: () => (       
                <NavBarButton 
                 location="right" 
                 color={colors.white} 
                 text="Log In" 
                 screenName = "Login"
                />   
             )         
            }} 
          //headerShown: false 
           />
           
        {/*<Stack.Screen name="Register" component={RegisterScreen} />*/}
        <Stack.Screen 
           name="Login" 
           component={LoginScreen}      
           options={{ 
            headerTransparent: true,
            headerTitle: "",
            headerTintColor: colors.white,
            headerStyle: transparentHeaderStyle,
            headerRight: () => (       
              <NavBarButton 
               location="right" 
               color={colors.white} 
               text="Forgot password" 
               screenName = "ForgotPassword"
              />   
           )                
           }} />

        <Stack.Screen 
           name="ForgotPassword" 
           component={ForgotPasswordScreen} 
           options={{ 
            headerTransparent: true,
            headerTitle: "",
            headerTintColor: colors.white,
            headerStyle: transparentHeaderStyle               
           }}/>

        <Stack.Screen 
           name="Register"
           component={RegisterScreen} 
           options={{ 
            headerTransparent: true,
            headerTitle: "",
            headerTintColor: colors.white,
            headerStyle: transparentHeaderStyle,
            headerRight: () => (       
              <NavBarButton 
               location="right" 
               color={colors.white} 
               text="Log In" 
               screenName = "Login"
              />   
           )                
           }}/>

         <Stack.Screen 
            name="Select_App" 
            component={Select_AppScreen} 
            options={{ 
              headerTransparent: true,
              headerTitle: "",
              headerTintColor: colors.black,
              headerStyle: transparentHeaderStyle,           
             }}/>
         <Stack.Screen 
            name="Created_App" 
            component={Created_AppScreen} 
            options={{ 
              headerTransparent: true,
              headerTitle: "",
              headerTintColor: colors.black,
              headerStyle: transparentHeaderStyle,         
             }}
            />
          <Stack.Screen 
            name="NoPolicy" 
            component={NoPolicyScreen} 
            options={{ 
              headerTransparent: true,
              headerTitle: Nondiscrimination_policy,
              headerTintColor: colors.black,
              headerStyle: transparentHeaderStyle,         
             }}
          />
            <Stack.Screen 
            name="PaymentsTofS" 
            component={PaymentsTofSScreen} 
            options={{ 
              headerTransparent: true,
              headerTitle: Payments_policy,
              headerTintColor: colors.black,
              headerStyle: transparentHeaderStyle,         
             }}
            />
              <Stack.Screen 
            name="PrivacyPolicy" 
            component={PrivacyPolicyScreen} 
            options={{ 
              headerTransparent: true,
              headerTitle: Privacy_policy,
              headerTintColor: colors.black,
              headerStyle: transparentHeaderStyle,         
             }}
            />
              <Stack.Screen 
            name="TermsofServices" 
            component={TermsofServicesScreen} 
            options={{ 
              headerTransparent: true,
              headerTitle: Terms_policy,
              headerTintColor: colors.black,
              headerStyle: transparentHeaderStyle,         
             }}
            />
      </Stack.Navigator>
    
    </NavigationContainer>
  )

}



export default App