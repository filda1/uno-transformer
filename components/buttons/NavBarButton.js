import React from 'react'
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native'
import { useNavigation } from '@react-navigation/native';



function GoToButton (props) {
  const navigation = useNavigation();

  const { screenName, content  } = props

  return (
  <TouchableOpacity 
    onPress={() => navigation.navigate(screenName)}>
    <Text> { content }</Text>  
   </TouchableOpacity>
  );
}


const NavBarButton = (props, { navigation }) => {

    const {
        location, 
        text, 
        color, 
        icon, 
        screenName,
        //handleButtonPress,
    } = props

    let content;

    const marginPosition = location === 'right' ? { marginRight: 20 } : { marginLeft: 20 }


    if (text) {
          content = (
          <Text style={[{ color }, marginPosition, styles.buttonText]}>
            {text}
          </Text>
        );
    } 
    else if (icon) {
          content = (
      <View style={marginPosition}>
        {icon}
      </View>
        );
    }
      return (
  
       <GoToButton screenName={screenName} content={content} />
        
      );

}

NavBarButton.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.object,
  //handleButtonPress: PropTypes.func.isRequired,
  location: PropTypes.string,
  color: PropTypes.string,
};

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 16,
  },
});





export default NavBarButton