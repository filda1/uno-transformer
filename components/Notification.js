import React,  { useState } from 'react'
import { PropTypes } from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Easing,
  Animated,
} from 'react-native';
import colors from '../styles/colors'

const Notification = (props) => {

    const {
        type, firstLine, secondLine, showNotification,
    } = props;


    const animateNotification = (value) => {
        Animated.timing(
            positionValue,
            {
              toValue: value,
              duration: 300,
              velocity: 3,
              tension: 2,
              friction: 8,
              easing: Easing.easeOutBack,
            },
          ).start();
    }

    const closeNotification = ()=> {
       props.handleCloseNotification();
    }

    const [ positionValue ,  sePositionValue ] = useState(new Animated.Value(-60))

    showNotification ? animateNotification(0) : animateNotification(-60);

    return (
        <Animated.View style={[{ marginBottom: positionValue }, styles.wrapper]}>
      <View style={styles.errorMessageContainer}>
        <View style={styles.errorMessage}>
          <Text style={styles.errorText}>
            {type}
          </Text>
          <Text>
            {firstLine}
          </Text>
        </View>
        <Text style={styles.errorMessage}>
          {secondLine}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.closeButton}
        onPress={closeNotification}
      >
        <Icon
          name="times"
          size={20}
          color={colors.lightGray}
        />
      </TouchableOpacity>
    </Animated.View>
    )
}


/*Notification.propTypes = {
    showNotification: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    firstLine: PropTypes.string,
    secondLine: PropTypes.string,
    handleCloseNotification: PropTypes.func,
  };*/
  
  const styles = StyleSheet.create({
    wrapper: {
      flex: 1,
      backgroundColor: colors.white,
      height: 60,
      padding: 10,
    },
    notificationContent: {
      flex: 1,
      flexDirection: 'column',
      flexWrap: 'wrap',
      alignItems: 'flex-start',
    },
    errorText: {
      color: colors.darkOrange,
      marginRight: 5,
      fontSize: 14,
      marginBottom: 2,
    },
    errorMessage: {
      flexDirection: 'row',
      flex: 1,
      marginBottom: 2,
      fontSize: 14,
    },
    errorMessageContainer: {
      flexDirection: 'row',
      flex: 1,
      marginBottom: 2,
    },
    closeButton: {
      position: 'absolute',
      right: 10,
      top: 10,
      zIndex: 999,
    },
  });


export default Notification
